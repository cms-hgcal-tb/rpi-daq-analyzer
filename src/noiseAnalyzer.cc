#include <noiseAnalyzer.h>

#include <sstream>
#include <fstream>
#include <algorithm>
#include <stdio.h>

#define IQR_TO_SIGMA 0.6745

noiseAnalyzer::noiseAnalyzer(std::string outputDir, std::string outputname, std::string mapfile)
{
  std::ostringstream os( std::ostringstream::ate );

  os.str("");
  os << outputDir << "/noise_" << outputname << ".txt";
  m_noiseFileForHexPlot = os.str();

  FILE* f = fopen(mapfile.c_str(), "r");
  if (f == 0) {
    fprintf(stderr, "Unable to open '%s'\n", mapfile.c_str() );
    exit(1);
  }
  char buffer[100];
  while (!feof(f)) {
    buffer[0] = 0;
    fgets(buffer, 100, f);
    if( std::string(buffer).find('#') != std::string::npos )
      continue;
    int ptr;
    int padID, chipID, chanID;
    const char* process = buffer;
    int found = sscanf(buffer, "%d %d %d %n ", &padID, &chipID, &chanID, &ptr);
    if (found == 3) {
      process += ptr;
    } else continue;
    chanID_to_padID ctp;
    ctp.chanID=chanID;
    ctp.chipID=chipID;
    ctp.padID=padID;
    m_padMap.insert( std::pair<int,chanID_to_padID>(padID,ctp) );
  }
}

void noiseAnalyzer::fillData( const std::vector<triggerHit>& hits )
{
  for( auto hit : hits ){
    for( int ts=0; ts<NUMBER_OF_TIME_SAMPLES-2; ts++ ){
      int key=100*100*hit.chip()+100*hit.channel()+ts;
      if( triggerhit_map.find(key)==triggerhit_map.end() ){
	triggerhit_data thd(key);
	triggerhit_map.insert( std::pair<int,triggerhit_data>(key,thd) );
      }
      triggerhit_map[key].highGain.push_back( hit.highGainADC(ts) );
      triggerhit_map[key].lowGain.push_back ( hit.lowGainADC(ts)  );
    }
  }
}

void noiseAnalyzer::analyze()
{
  std::ofstream out;
  out.open( m_noiseFileForHexPlot.c_str(), std::ios::out );
  out << "## selector (0-9 noise LG, 10-19 noise HG ) pad value" << std::endl;

  for( auto tcd : triggerhit_map ){
    triggerhit_data triggerhit=tcd.second;
    int chip = triggerhit.key/100/100;
    int chan = (triggerhit.key/100)%100;
    int ts = triggerhit.key%100;
    
    int padID=-1;
    for(auto pad:m_padMap)
      if( pad.second.chipID==chip && pad.second.chanID==chan ){
    	padID=pad.second.padID;
    	break;
      }

    auto hg=triggerhit.highGain;
    auto lg=triggerhit.lowGain;
    std::sort( hg.begin(), hg.end() );
    std::sort( lg.begin(), lg.end() );

    unsigned int size = hg.size();
    int medianIndex = int( 0.5*(size-1) );
    int sigma_1_Index = int( 0.25*(size-1) );
    int sigma_3_Index = int( 0.75*(size-1) );
    triggerhit.medianHG = hg[ medianIndex ];
    triggerhit.medianLG = lg[ medianIndex ];
    
    triggerhit.iqrHG=0.5*( hg[ sigma_3_Index ]-hg[ sigma_1_Index ])/IQR_TO_SIGMA;
    triggerhit.iqrLG=0.5*( lg[ sigma_3_Index ]-lg[ sigma_1_Index ])/IQR_TO_SIGMA;
    
    if( padID==-1 )
      continue;
    out << ts << "\t" << padID << "\t" << triggerhit.iqrLG << std::endl;
    out << 10+ts << "\t" << padID << "\t" << triggerhit.iqrHG << std::endl;
  }
  out.close();
}
