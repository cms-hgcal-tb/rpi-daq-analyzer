#include <occupancyAnalyzer.h>
#include <occupancyData.h>
#include <fstream>
#include <iostream>

#include <sstream>
#include <algorithm>

occupancyAnalyzer::occupancyAnalyzer(std::string outputDir, std::string outputname, std::string mapfile, int threshold, int expectedMaxTS, int maxHitsPerEv)//std::string outputname, std::string mapfile, int maxTS)
{
  m_threshold = threshold;
  m_expectedMaxTS = expectedMaxTS > 0 ? expectedMaxTS : expectedMaxTS+1 ;
  m_maxHitsPerEv = maxHitsPerEv;

  std::ostringstream os( std::ostringstream::ate );

  os.str("");
  os << outputDir << "/occupancy_" << outputname << ".ar";
  m_occupancyFile = os.str();

  os.str("");
  os << outputDir << "/occupancy_" << outputname << ".txt";
  m_occupancyFileForHexPlot = os.str();

//   m_maxTS=maxTS;

  FILE* f = fopen(mapfile.c_str(), "r");
  if (f == 0) {
    fprintf(stderr, "Unable to open '%s'\n", mapfile.c_str() );
    exit(1);
  }
  char buffer[100];
  while (!feof(f)) {
    buffer[0] = 0;
    fgets(buffer, 100, f);
    char* p_comment = index(buffer, '#');
    if (p_comment != 0) *p_comment = 0;
    int ptr;
    int padID, chipID, chanID;
    const char* process = buffer;
    int found = sscanf(buffer, "%d %d %d %n ", &padID, &chipID, &chanID, &ptr);
    if (found == 3) {
      process += ptr;
    } else continue;
    m_padMap.insert( std::pair<int,int>(100*chipID+chanID,padID) );
  }
}

void occupancyAnalyzer::fillData( std::vector<triggerHit> hits )
{
  std::vector<int> m_hg; //[NUMBER_OF_TIME_SAMPLES];

  // first, loop on hits and remove events with, overall, > m_maxHitsPerEv hits
  bool skipEvent = false;
  int n_hit_perEv  = 0;
  for( auto hit : hits ){
    n_hit_perEv += hit.highGainADC(m_expectedMaxTS)+
                   hit.highGainADC(m_expectedMaxTS+1)-
                   0.6*hit.highGainADC(m_expectedMaxTS+3)> m_threshold ? 1 : 0 ;
    if (n_hit_perEv > m_maxHitsPerEv) {
      skipEvent = true; 
      break;
    }
  }
  if (skipEvent) return;
  
  for( auto hit : hits ){
    m_hg.clear();
    int key=100*hit.chip()+hit.channel();
    if( hit_data_map.find(key)==hit_data_map.end() ){
      hit_channel_data hcd(key);
      hit_data_map.insert( std::pair<int,hit_channel_data>(key,hcd) );
    }

    for( unsigned int it=0; it<NUMBER_OF_TIME_SAMPLES; it++ ){
      m_hg.push_back( hit.highGainADC(it) );
    }
    hit_data_map[key].highGain.push_back( m_hg );
  }

}

void occupancyAnalyzer::analyze()
{
  occupancyData _occData;
  for( auto hit : hit_data_map ){
    hit_channel_data hit_channel=hit.second;
    int chip = hit_channel.key/100;
    int chan = (hit_channel.key)%100;
   
    int padID=-1;
    for(auto pad:m_padMap)
      if( pad.first/100==chip && pad.first%100==chan ){
    	padID=pad.second;
    	break;
      }

    if( _occData.contain(chip,chan)==false ){
      _occData.fillData(chip,chan,padID);
    }
    
    if (padID == -1) continue;

    auto occD = _occData.get(chip,chan);
    int nhits = occD.getNHits();
    // remove events where nhits per event is > 10
    for (unsigned int ihit = 0; ihit < hit_channel.highGain.size(); ihit ++){
        nhits += hit_channel.highGain[ihit][m_expectedMaxTS]+ 
                 hit_channel.highGain[ihit][m_expectedMaxTS+1]-
                 0.6*hit_channel.highGain[ihit][m_expectedMaxTS+3] > m_threshold ? 1 : 0 ;
    }
    occD.setNHits(nhits);
    _occData.update(occD);
  }
  
  std::ofstream out;
  out.open( m_occupancyFileForHexPlot.c_str(), std::ios::out );
  
  out << "## selector (nhits base selection) pad value" << std::endl;
  // NHITS 
  //   int counter = 0;
  for( auto ocd : _occData.get() ){
    if( ocd.sensorpad()!=-1 )
      out << "0" << "\t" << ocd.sensorpad() << "\t" << ocd.getNHits() << std::endl;
//     counter++;
  }
  out.close();
}
  
