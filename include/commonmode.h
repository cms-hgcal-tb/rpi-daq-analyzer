#ifndef COMMONMODE
#define COMMONMODE

#include "triggerHit.h"

#include <iostream>
#include <vector>
#include <map>

#define NUMBER_OF_CONNECTED_CHANNELS 198

class commonmode{
 public:
  commonmode(){;}
  commonmode( std::string mapfile, int expectedMaxTS, int threshold );
  ~commonmode(){}

  void evalAndSubtractCommonMode( std::vector<triggerHit>& hits );

 private:
  static triggerHit subtractCM(triggerHit hit,std::vector<float> cmhigh, std::vector<float> cmlow);
  
  std::vector<int> m_connectedChannelKeys;
  float m_threshold;
  int m_expectedMaxTS;

  std::vector<float> m_adcHigh[NUMBER_OF_TIME_SAMPLES];
  std::vector<float> m_adcLow[NUMBER_OF_TIME_SAMPLES];
};

#endif
