# Download 

git clone https://gitlab.cern.ch/asteen/rpi-daq-analyzer.git

# Prerequisites

cmake, c++11, root, boost

# Installation

```
mkdir build
cd build
cmake ../
make install -j4
cd ..
```

# Running the analyzer
Print all available options:
```
./bin/hexaboardAnalyzer --help
```

## Running analysis on pedestal raw data file:
```
mkdir results #default location for output files
./bin/hexaboardAnalyzer -f pedestal_run.raw -a pedestal -o some_name --maxTS=5 -m pad_to_channel_map_8inch_198.csv --hexaboardType=8inch #for 8" module data
./bin/hexaboardAnalyzer -f pedestal_run.raw -a pedestal -o some_name --maxTS=5 -m chanID_to_pad.txt --hexaboardType=6inch #for 6" module data
```

The analyzer creates 1 root file (some_name.root) with 1 TTree containing the raw data from the Skiroc2-CMS chip, 1 txt file (pedestal_some_name.txt) containing the pedestal and noise values for all channels (also including the non-connected channels) for both gain, and 1 .ar file (pedestal_some_name.ar) containg also pedestal and noise values.

An entry of the TTree corresponds to the raw data of 1 Skiroc2-CMS ASIC. The tree contains:

event

chip

roll # the roll mask value, used to re-order SCA in time

dacinj # value of the injection in DAC unit

timesamp[NSCA] # array with 13 entries : timesamp[5]=2 means the timesample of SCA5 is 2

hg[NSCA][NCHANNELS] # raw high gain value for each channel and each SCA

lg[NSCA][NCHANNELS] # raw low gain value for each channel and each SCA

tot_fast[NCHANNELS] # tot fast ramp for each channel, never used in analysis

tot_slow[NCHANNELS] # tot slow ramp for each channel

toa_rise[NCHANNELS] # toa rise for each channel

toa_fall[NCHANNELS] # toa fall for each channel


The txt file format is the following :

`selector   padID   value ## selector (0-12 pedestal LG, 100-112 pedestalHG, 200-212 noise LG, 300-312 noise HG ) pad value`

This format was chosen for the HexPlot tool (https://gitlab.cern.ch/CLICdp/HGCAL/HGCAL_sensor_analysis) used to plot pedestal/noise/current/capacitance with a nice sensor geometry.
To run the Hexplot tool on our pedestal files : 

```
./bin/HexPlot -i ../rpi-daq-analyzer/results/pedestal_some_name.txt -g geo/hex_positions_HPK_198ch_8inch_testcap.txt  -o examples/some_name.png --noinfo --vn current:Noise:"ADC counts" -z 0:20 --select 300
```
would plot the high gain noise of SCA 0 (using here the 8" sensor geometry).


The file pedestal_some_name.ar is used for pedestal subtraction in other analysis.

## Running analysis on cosmic raw data file:
```
./bin/hexaboardAnalyzer -f cosmic_run.raw -a occupancy -o occupancy_name -m pad_to_channel_map_8inch_198.csv --hexaboardType=8inch -p results/pedestal_some_name.ar #for 8" module data
./bin/hexaboardAnalyzer -f cosmic_run.raw -a occupancy -o occupancy_name -m chanID_to_pad.txt --hexaboardType=6inch -p results/pedestal_some_name.ar --threshold=40 --expectedMax=3 --maxNHits=10 #for 6" module data
```

This will create 1 root file (occupancy_name.root) and 1 txt file (occupancy_occupancy_name.txt). The .txt file format is the same as for the pedestal analysis, to be used with the hexplot tool.
An entry in the root file corresponds to 1 event for the full module. The format is the following : 

event

nhit # number of hits : a channel has a hit if `hg[expectedMax] + hg[expectedMax+1] - 0.6 hg[expectedMax+3] > threshold`

chip[NCHANNELS] # chip ID of the channels , here NCHANNELS is 256 (64*4)

channels[NCHANNELS] # channel ID of the channels : the channel ID goes from 0 to 63

pad[NCHANNELS] # pad ID of the channels : pad identifier of the sensor, pad ID set to -1 for non connected channels 

hg[TIMESAMP][NCHANNELS] # high gain of the channels after pedestal and CM subtraction for each time sample (going from 0 to 10)

lg[TIMEAMP][NCHANNELS] # low gain of the channels after pedestal and CM subtraction for each time sample (going from 0 to 10)

tot_fast[NCHANNELS] # tot fast ramp for each channel, never used in analysis

tot_slow[NCHANNELS] # tot slow ramp for each channel

toa_rise[NCHANNELS] # toa rise for each channel

toa_fall[NCHANNELS] # toa fall for each channel
